walk_workdays_(=, 0, Year, Month, Day) :-
    % end of walk, format end date and print.
    Date = date(Year, Month, Day),
    format_time(atom(Out), '%A, %d%m', Date),
    writeln(Out).

walk_workdays_(<, N, Year, Month, Day) :-
    % Increment the day. NB. it could go past end of month;
    % round tripping through a timestamp fixes that, e.g.
    % 2023-01-32 becomes
    % 2023-02-01
    succ(Day, Day1),
    date_time_stamp(date(Year, Month, Day1), Stamp),
    stamp_date_time(Stamp, date(Year2, Month2, Day2, _,_,_, _,_,_), local),

    % Only decrement counter on days 1-5 weekdays, not 6-7 weekends.
    day_of_the_week(date(Year2, Month2, Day2), DotW),

    (DotW < 6
     -> succ(N2, N)
     ;  N2 = N),

    walk_workdays(N2, Year2, Month2, Day2).

% Wrapper to leave no choicepoints.
walk_workdays(N, Year, Month, Day) :-
    compare(Order, 0, N),
    walk_workdays_(Order, N, Year, Month, Day).

n_work_days(Date, N) :-
    % Parse Date "2205" into Day 22 and Month 05
    sub_string(Date, 0, 2, _, StrDay),
    sub_string(Date, 2, 2, _, StrMonth),
    number_string(Day, StrDay),
    number_string(Month, StrMonth),

    % walk N workdays from now and print where it ends.
    walk_workdays(N, 2023, Month, Day).