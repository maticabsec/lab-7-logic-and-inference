# Lab 7 - Logic and Inference

Solve one of the following problems using Prolog. Given a day and a date, what will be the day and date after days of working day.
Assumptions:

There are 5 working days in a week

The year is 2023

N <= 365

Example:

% what will be the day and date after 6 working days from 22nd of May ?-

n_work_days("2205", 6)

"Tuesday, 3005"

% what will be the day and date after 10 working days from 1st of June ?

n_work_days("0106", 10)

"Thursday, 1506"